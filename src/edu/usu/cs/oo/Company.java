package edu.usu.cs.oo;

public class Company {

	private String name;
	private Location location;
	
	
	public Company(String myName, Location myLocation)
	{
		name = myName;
		location = myLocation;
	}
	
	public void setName(String myName)
	{
		name = myName;
	}
	public void setLocation(Location myLocation)
	{
		location = myLocation;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Location getLocation()
	{
		return location;
	}
	
	public String toString()
	{
		return (name + ", " + location.getStreetAddress() + ", " + location.getAreaCode() + ", " + location.getCity() + ", " + location.getState());
	}
	
	/*
	 * Create the constructor, getters, setters, and anything else
	 * that is necessary to make Company work.
	 * 
	 * Note: This includes the toString() method.
	 */
}
