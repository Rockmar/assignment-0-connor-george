package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		Location location = new Location("1234 Walnut Street", "12345", "Logan", "Utah");
		Student student = new Student("Connor George", "A01234567", new Job("Computer Scientist", 999999, new Company("usu", location)));
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		
		
		System.out.println(student);
		
		/*
		 * Print out the student information. 
		 */
	}

}