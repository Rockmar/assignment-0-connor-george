package edu.usu.cs.oo;

public class Location {

	private String streetAddress;
	private String areaCode;
	private String city;
	private String state;
	
	/*
	 * Create a constructor here that allows you to initialize a Location
	 * with streetAddress, areaCode, city, and state.
	 */
	public Location(String myAddress, String myAreaCode, String myCity, String myState)
	{
		streetAddress = myAddress;
		areaCode = myAreaCode;
		city = myCity;
		state = myState;
	}
	
	public void setStreetAddress(String address)
	{
		streetAddress = address;
	}
	public void setAreaCode(String myAreaCode)
	{
		areaCode = myAreaCode;
	}
	public void setCity(String myCity)
	{
		city = myCity;
	}
	public void setState(String myState)
	{
		state = myState;
	}
	
	public String getStreetAddress()
	{
		return streetAddress;
	}
	public String getAreaCode()
	{
		return areaCode;
	}
	public String getCity()
	{
		return city;
	}
	public String getState()
	{
		return state;
	}
	
	
	/*
	 * Create getters and setters here for retrieving the private member variables here. 
	 */
}
